import React from 'react'

import classes from './Input.module.css'

const Input = (props) => {

  return (
    <div className={classes.input}>
      <label htmlFor={props.input.id}>{props.label}</label>
      <input {...props.input} /> {/* all key-value props will be add as a property */}
    </div>
  )
}

export default Input