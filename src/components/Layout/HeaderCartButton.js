import React from 'react'

import Carticon from '../Cart/Carticon'
import classes from './HeaderCartButton.module.css'

const HeaderCartButton = (props) => {
  return (
    <button className={classes.button} onClick={props.onClick}>
      <span className={classes.icon}><Carticon /></span>
      <span>Your cart</span>
      <span className={classes.badge}>3</span>
    </button>
  )
}

export default HeaderCartButton